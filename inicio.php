<!DOCTYPE html>
<?php 
include_once 'includes/nucleo.php';
include_once 'includes/conexiones.php';
include_once 'includes/modales.php';

$ftp_server = "ftp.simetrical.org";
$conn_id = ftp_connect($ftp_server);
$ftp_user_name = "process";
$ftp_user_pass = "40b67c197405";
$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

if (!$conn_id) { 
	echo "Imposible conectar :(";
	exit; 
} else {
	ftp_pasv($conn_id, true);
	$contents = ftp_nlist($conn_id, '/originales');
	for ($i = 0 ; $i < count($contents) ; $i++){
          //echo "<a href=".substr($contents[$i],1).">" . substr($contents[$i],1) . "</a><br>";
	}
	?>

	<html>
	<head>
		<link rel="stylesheet" type="text/css" href="css/catalogo.css">	
		<title>Home</title>
	</head>

	<body>


		<!-- MODAL ORIGINALES -->
		<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-dialog-scrollable modal-xl">
				<div class="modal-content" style="background-color: #24283C!important; color: white;">
					<div class="modal-header">
						<h6 class="modal-title">Información Paquetes Originales</h6>
					</div>
					<div class="modal-body">

						<span>Cuando el MonServer los toma de esta ruta, el paquete se mueve de ruta, por lo tanto, este listado sufre cambios con forme se procesa en MonServer y los mueve a <strong>/Convertidos</strong>. Actualmente se visualizan todos los paquetes de la ruta, no únicamente los del día.</span><br><br><br>
						<strong>Listado de Paquetes en /Originales</strong><br><br>

						<?php 
						for ($i = 0 ; $i < count($contents) ; $i++){
							echo substr($contents[$i],1) . "<br>";
						}
						?> <hr>

						<?php 
						$cO = [];
						for ($i = 0 ; $i < count($contents) ; $i++){
							$clientReg1 = substr($contents[$i],12,4);
							array_push($cO, $clientReg1);
						}
						$nuevoCo = array_count_values($cO);
						//print_r($cO);
						//echo '<hr>';
						//print_r($nuevoCo);
						//echo '<hr>';

						$conteoOriginales1 = count($nuevoCo);
						//echo "<hr>";
						//echo "de <strong>". ($conteoOriginales1) ."</strong> clientes ";
						//echo "<hr>";


						//$i3 = 0;
						?>



						
					</div>
					<div class="modal-footer">
					</div>
				</div>
			</div>
		</div> <!-- fin del modal Originales -->

		<!-- contenedor general -->
		<div class="container"><br>


			<!-- barra de navegación -->
			<nav class="navbar navbar-expand-lg shadow rounded" id="nav1">
				<a class="navbar-brand"  id="titulo1">S I M P R O C E S S</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="navbarText">
					<ul class="navbar-nav mr-auto">
						<li class="nav-item active">
							<a class="nav-link" href="#" style="color: white;"> Dashboard <span class="sr-only">(current)</span></a>
						</li>
					</ul>
					<span class="navbar-text" style="padding: 0px;">
						<div class="dropdown">
							<a class="btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white; font-size: 11px;">
								Sesión
							</a>

							<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
								<a class="dropdown-item" href="#" style="font-size: 12px;">Cerrar sesión</a>
							</div>
						</div>
					</span>
				</div>
			</nav> <!-- fin de barra de navegación -->
			<br>

			<!-- segunda fila para información de clientes -->
			<div class="row">
				<div class="col">
					<span style="color:white;">
						<?php
						date_default_timezone_set('America/Mexico_City');
						if ($paqR <  20 ) {
							echo 'Problema MonServer, pocos paquetes procesados';
						} else {
							echo date('l jS \of F Y h:i:s A');
						}
						?>
					</span>
				</div>
				<div class="col">

				</div>
				<div class="col text-right">
					<span style="color:white;"><img src="images/switch-on.svg" width="19px;" style="margin-right: 10px;">
						<strong style="font-size: 14px; color: white;" class="text-right">327</strong> Clientes activos  [ <strong><?php echo $respSolEspe; ?></strong> Esperados ]</span>
					</div>
				</div>

				<!-- cierre de información de clientes --> <br>


				<div class="row" style="margin: 0px!important;">
					<div class="col-2" style="background-color: #24283C!important; margin-right: 10px; margin-left: 0px; border-radius: 4px;">
						<!-- card paquetes nuevo -->
						<ul class="list-group" >
							<li class="list-group-item" style="background-color: #24283C!important; font-weight: bold; font-size: 16px; color:white;">
								PAQUETES
							</li>

							<li class="list-group-item"  style="padding:5px; cursor: hand; background-color: #24283C!important;">
								<div class="container" style="text-align: center; padding: 0px; color:white;">
									<h5 data-toggle="modal" data-target="#exampleModal" style="cursor: pointer;"> <?php echo $i; ftp_close($conn_id);}?> </h5>
									<img src="images/pending.svg" width="21px;"> <strong> /Originales</strong>
									<p style="font-size: 12px; color: #78909C; font-weight: lighter;">de <?php echo $conteoOriginales1; ?> Clientes</p>
								</div>
							</li>

							<li class="list-group-item"  style="padding:5px; cursor: hand; background-color: #24283C!important;">
								<div class="container" style="text-align: center; padding: 0px; color:white;">

									<h5 data-toggle="modal" data-target="#modalConvertidos" style="cursor: pointer;"> <?php echo $paqR; ?> </h5>
									<img src="images/box.svg" width="21px;"> <strong> /Convertidos</strong>	
									<p style="margin-bottom: 4px; font-size: 12px; color: #78909C; font-weight: lighter;"><?php echo $clt_paq_dist; ?> Clientes</p>
									<p style="font-size: 12px; color: #78909C; font-weight: lighter;" data-toggle="tooltip" data-placement="right" title="Clientes esperados - Clientes convertidos">
										<?php
										$pistol = $respSolEspe - $clt_paq_dist;
										echo $pistol; 
										?> Diferencia 	</p>
									</div>
								</li>

							</ul>

							<!-- fin del pauetes nuevos -->


							<br>

						</div> <!-- fin de col1 -->


						<div class="col-3" style="background-color: #24283C!important; margin-left: 6px; border-radius: 4px;">

							<div class="card">
								<ul class="list-group list-group-flush">
									<li class="list-group-item" style="background-color: #24283C!important; font-weight: bold; font-size: 16px;">
										PRDSERVER´S
										<p style="font-size: 12px; color: #78909C; font-weight: lighter; margin-bottom: 0px;">
											Datos de los Servidores levantados hoy
										</p>
									</li>
									<li class="list-group-item" style="background-color: #24283C!important; cursor: pointer;" data-toggle="modal" data-target="#modalSolicitudes">
										<img src="images/catalogue.png" width="19px;" style="margin-right: 10px;">
										<strong style="font-size: 14px;"> <?php echo $resp_sol_realizadas; ?> </strong> Registrados</li>

										<li class="list-group-item" style="background-color: #24283C!important;">
											<img src="images/boxpen.svg" width="19px;" style="margin-right: 10px;">
											<strong style="font-size: 14px;"><?php echo $imp_sol_pend; ?> </strong> Pendientes</li>

											<!--<li class="list-group-item" style="background-color: #24283C!important;" data-toggle="tooltip" data-placement="right" title="Clientes convertidos - Solicitudes registradas">
												<img src="images/target.svg" width="19px;" style="margin-right: 10px;">
												<strong style="font-size: 14px;"><?php $diferencia = $clt_paq_dist - $resp_sol_realizadas; //echo $diferencia; ?></strong> Faltantes</li> -->

												<li class="list-group-item" style="background-color: #24283C!important;">
													<span class="badge" id="estatus1" style="margin-bottom: 0px;">ESTATUS</span> 
												</li>

												<li class="list-group-item" style="background-color: #24283C!important;">
													<img src="images/processing.svg" width="19px;" style="margin-right: 10px;">
													<strong style="font-size: 14px;"><?php echo $clientesRegistrados - $clientesFinalizados; ?></strong> Procesando</li>

													<li class="list-group-item" style="background-color: #24283C!important; cursor: pointer;" data-toggle="modal" data-target="#modalEstatus">
														<img src="images/correct.svg" width="19px;" style="margin-right: 10px;">
														<strong style="font-size: 14px;"><?php echo $clientesFinalizados; ?></strong> Finalizados</li>

														<li class="list-group-item" style="background-color: #24283C!important;">
															<span class="badge" id="estatus1" style="margin-bottom: 0px;">ERRORES</span> 
														</li>

														<li class="list-group-item" style="background-color: #24283C!important;">
															<img src="images/online-recruitment.svg" width="19px;" style="margin-right: 10px;">
															<strong style="font-size: 14px;"> <?php echo $imp_clte; ?> </strong> Clientes</li>

															<li class="list-group-item" style="background-color: #24283C!important; cursor: pointer;"  data-toggle="modal" data-target="#modalErrores">
																<img src="images/cancel.svg" width="19px;" style="margin-right: 10px;">
																<strong style="font-size: 14px;"><?php echo $imp_error; ?></strong> Totales</li>

															</ul>
														</div>


													</div><!-- cierre de los 2 kpi medio -->








													<div class="col" style="padding-right: 0px;"> <!-- tercera columna -->

														<div class="card">
															<ul class="list-group list-group-flush">
																<li class="list-group-item" style="background-color: #24283C!important; font-weight: bold; font-size: 16px;">
																	HISTÓRICO
																	<p style="font-size: 12px; color: #78909C; font-weight: lighter; margin-bottom: 0px;"> Últimos 30 días</p>
																</li>
															</ul>
															<div id="chartdiv2" style="height: 240px;"></div>
														</div>
														<br>

														<div class="card">
															<ul class="list-group list-group-flush">
																<li class="list-group-item" style="background-color: #24283C!important; font-weight: bold; font-size: 14px;">
																	ZONAS HORARIAS
																</li>

															</ul>
															<div id="chartdiv"></div>

														</div>

														<br>



																<!-- comentamos marcas	<div class="card">
																		<ul class="list-group list-group-flush">
																			<li class="list-group-item" style="background-color: #24283C!important; font-weight: bold; font-size: 14px;">
																				MARCAS
																				<p style="font-size: 14px; color: #78909C; font-weight: lighter;"> <?php //echo $imprimirMarcas; ?></p>
																			</li>
																		</ul>
																	</div> -->



																</div> <!-- fin de la tercera columna -->
															</div>



															<br><br>

															<!-- AQUI VA LA TABLA GENERAL -->
															<!-- TABLA GENERAL -->


															<table class="table table-sm table-borderless table-striped mb-0" id="tablaClientesCentral" name="tablaClientesCentral">
																<thead style="font-size: 12px;  background-color: transparent!important; color:white!important;">
																	<tr>
																		<th>Cve</th>
																		<th>Grupo</th>
																		<th>Branchs</th>
																		<th>Paquetes</th>
																		<th>Tipo</th>
																		<th>GMT</th>
																		<th>Procesó</th>
																		<th>Estatus</th>
																		<TH>Inf</TH>
																	</tr>
																</thead>

																<tbody style="font-size: 12px!important; background: #24283C; color:white; font-size: 11px!important;">
																	<?php

																	while($fila = mysqli_fetch_array($ejecutaCatalogo))
																	{
																		?>
																		<tr>
																			<td><strong><?php echo ($fila['Client']);?></strong></td>
																			<td><?php echo ($fila['Nombre']);?></td>
																			<td><?php echo $fila['Branch'] ?></td>
																			<td><?php echo $fila['Paquetes'] ?></td>
																			<td><?php echo ($fila['TipoCliente']) ?></td>

																			<td>
																				<?php if($fila['GMT'] == "America/Mexico_City") {  ?>
																					<a class="transparent" style="color: #c2c2c2;">
																						<img src="images/mexico.png" style="width: 23px; padding-right: 10px;"><?php echo $fila['GMT']; ?>
																					</a>
																				<?php } else if ($fila['GMT'] == "America/Santiago")  {  ?>
																					<a class="transparent" style="color: #c2c2c2;">
																						<img src="images/chile.png" style="width: 23px; padding-right: 10px;"><?php echo $fila['GMT']; ?>
																					</a>
																				<?php } else if ($fila['GMT'] == "America/Argentina/Buenos_Aires") { ?>
																					<a class="transparent" style="color: #c2c2c2;">
																						<img src="images/argentina.png" style="width: 23px; padding-right: 10px;"><?php echo $fila['GMT']; ?>
																					</a>


																					<?php
																				} else if ($fila['GMT'] == "America/Sao_Paulo") { 

																					echo '<img src="images/brasil.png" style="width: 23px; padding-right: 10px;">'.$fila['GMT'];


																				}  else if ($fila['GMT'] == "Central European Time") { 
																					echo '<img src="images/union-europea.png" style="width: 23px; padding-right: 10px;">'.$fila['GMT'];
																				} else { 
																					echo $fila['GMT'];
																				} ?>
																			</td>

																			<td>
																				<?php

																				$fechaHoy = gmDate("Y-m-d");
																				$fechaProcesoEl = substr($fila['fechaCompletado'],0,-8); 
																				$fechaProceso = date("Y-m-d", strtotime( $fechaProcesoEl));


																				$fechaProcesamientos = substr($fila['fechaProcesamientos'], 0,-8);
																				$fechaEnProcesamientos = date("Y-m-d", strtotime($fechaProcesamientos));


																				if ($fechaHoy == $fechaProceso) {
																					echo '<a style="color: #1de9b6;">'.$fila['fechaCompletado'].'</a>';
																				}  else {
																					echo '<a style="color: #e57373;">'.$fechaProcesoEl.'</a>';

																				}
																				?>

																			</td>

																			<td>
																				<?php 

																				if ($fechaHoy == $fechaProceso) {
																					echo '<a style="color: #1de9b6;"><img src="images/switch-on.svg" width="17px;"> Finalizado</a>';
																				} else if ($fechaHoy == $fechaEnProcesamientos) {
																					echo  '<a style="color: #c2c2c2;"><img src="images/processing.svg" width="15px;"> Trabajando</a>';

																				} else {
																					echo '<a style="color: #e57373;"><img src="images/cancel.svg" width="15px;"> No inició</a>';
																				}
																				?>
																			</td>

																			<td><a href='detalle.php?Cve=<?php echo $fila['Client'] ?>&Grupo=<?php echo $fila['Nombre'] ?>&Paquetes=<?php echo $fila['Paquetes'] ?>' class="waves-effect waves-blue btn-flat btn-small"><img src="images/plus.svg" width="15px;"></a></td>
																		</tr>
																		<?php
																	}
																	?>
																</tbody>
															</table> 




														</div> 
















													</body>

													<script type="text/javascript">

														$(document).ready(function(){
															var table = $('#tablaClientesCentral').DataTable( {
																bPaginate: false,
																orderCellsTop: false,
																fixedHeader: true
															} );

															$('#tablaClientesCentral thead tr').clone(true).appendTo('#tablaClientesCentral thead');

															$('#tablaClientesCentral thead tr:eq(1) th').each( function (i) {
																var title = $(this).text();
																$(this).html(' <input type="text" style="color: white;" /> ');

																$('input', this).on('keyup change', function() {
																	if(table.column(i).search() !== this.value ) {
																		table 
																		.column(i)
																		.search(this.value)
																		.draw();
																	}
																});
															});

														});

													</script>


													<!-- Button trigger modal -->
													<!-- fin de contenedor general -->

													<script>
														am4core.ready(function() {

// Themes begin
am4core.useTheme(am4themes_dark);
am4core.useTheme(am4themes_animated);
// Create map instance
var chart = am4core.create("chartdiv", am4maps.MapChart);
// Set map definition
chart.geodata = am4geodata_worldLow;
// Set projection
chart.projection = new am4maps.projections.Miller();
// Create map polygon series
var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

// Exclude Antartica
polygonSeries.exclude = ["AQ"];

// Make map load polygon (like country names) data from GeoJSON
polygonSeries.useGeodata = true;

// Configure series
var polygonTemplate = polygonSeries.mapPolygons.template;
polygonTemplate.tooltipText = "{name}";
polygonTemplate.polygon.fillOpacity = 0.6;


// Create hover state and set alternative fill color
var hs = polygonTemplate.states.create("hover");
hs.properties.fill = chart.colors.getIndex(0);

// Add image series
var imageSeries = chart.series.push(new am4maps.MapImageSeries());
imageSeries.mapImages.template.propertyFields.longitude = "longitude";
imageSeries.mapImages.template.propertyFields.latitude = "latitude";
imageSeries.mapImages.template.tooltipText = "{title}";
imageSeries.mapImages.template.propertyFields.url = "url";

var circle = imageSeries.mapImages.template.createChild(am4core.Circle);
circle.radius = 3;
circle.propertyFields.fill = "color";

var circle2 = imageSeries.mapImages.template.createChild(am4core.Circle);
circle2.radius = 3;
circle2.propertyFields.fill = "color";


circle2.events.on("inited", function(event){
	animateBullet(event.target);
})


function animateBullet(circle) {
	var animation = circle.animate([{ property: "scale", from: 1, to: 1 }, { property: "opacity", from: 1, to: 0 }], 1000, am4core.ease.circleOut);
	animation.events.on("animationended", function(event){
		animateBullet(event.target.object);
	})
}

var colorSet = new am4core.ColorSet();

imageSeries.data = [
<?php while($row = mysqli_fetch_array($datosGMT)) { ?>
	{
		"title": "<?php echo $row['GMT']; ?>",
		"latitude": <?php echo $row['latitud']; ?>,
		"longitude": <?php echo $row['longitud']; ?>,
		"color":colorSet.next()
		
	},
<?php }  ?>

];



}); // end am4core.ready()


// SEGUNDA GRAFICA =================
am4core.ready(function() {
	am4core.useTheme(am4themes_animated);
	var chart = am4core.create("chartdiv2", am4charts.XYChart);

	chart.data = [
	<?php while($rowf = mysqli_fetch_array($datosFecha)) { ?>
		{
			"date": "<?php echo $rowf['todasFechas']; ?>",
			"value": <?php echo $rowf['TotalClientes'] ?>
		},
	<?php  } ?>
	];


	chart.dateFormatter.inputDateFormat = "yyyy-MM-dd";


	var dateAxis = chart.xAxes.push(new am4charts.DateAxis());
	var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());


	var series = chart.series.push(new am4charts.LineSeries());
	series.dataFields.valueY = "value";
	series.dataFields.dateX = "date";
	series.fontSize = 10;
	series.tooltipText = "{value}"
	series.strokeWidth = 2;
	series.minBulletDistance = 15;
	series.stroke = am4core.color("#00695C"); // red

	series.tooltip.background.cornerRadius = 20;
	series.tooltip.background.strokeOpacity = 0;
	series.tooltip.pointerOrientation = "vertical";
	series.tooltip.label.minWidth = 40;
	series.tooltip.label.minHeight = 40;
	series.tooltip.label.textAlign = "middle";
	series.tooltip.label.textValign = "middle";

// Make bullets grow on hover
var bullet = series.bullets.push(new am4charts.CircleBullet());
bullet.circle.strokeWidth = 1;
bullet.circle.radius = 2;
bullet.circle.fill = am4core.color("#00695C");

var bullethover = bullet.states.create("hover");
bullethover.properties.scale = 1.2;

// Make a panning cursor
chart.cursor = new am4charts.XYCursor();
chart.cursor.behavior = "panXY";
chart.cursor.xAxis = dateAxis;
chart.cursor.snapToSeries = series;

dateAxis.keepSelection = true;
}); // end am4core.ready()
</script>




</body>
</html>