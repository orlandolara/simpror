<?php 

$server = 'datatest.simetrical.net';
$user = 'olara';
$pass = 'SIMc05e509298a3';
$db = null;
$port = '5123';

//string de conexión
$conexion = new mysqli($server, $user, $pass, $db, $port);

if($conexion -> connect_errno){
	die($conexion -> connect_errno);

}
//GRAFICAS
$datosGMT = mysqli_query($conexion, "SELECT DISTINCT(GMT), 
	(CASE WHEN GMT = 'America/Mexico_City'            then 23.634501 
	WHEN GMT = 'America/Santiago'               then -33.45694 
	WHEN GMT = 'America/Argentina/Buenos_Aires' then -38.416097
	WHEN GMT = 'America/Sao_Paulo'              then -23.5475
	WHEN GMT = 'Central European Time'          then 40.463667
	WHEN GMT = 'Europe/Madrid'                  then 40.4165
	END) AS latitud,
	(CASE WHEN GMT = 'America/Mexico_City'            then -102.552784 
	WHEN GMT = 'America/Santiago'               then -70.648274
	WHEN GMT = 'America/Argentina/Buenos_Aires' then -63.616672
	WHEN GMT = 'America/Sao_Paulo'              then -46.63611
	WHEN GMT = 'Central European Time'          then -3.74922 
	WHEN GMT = 'Europe/Madrid'                  then -3.70256
	END) AS longitud
	FROM crm_simetrical.groups WHERE GMT != ''");

$datosFecha = mysqli_query($conexion, "SELECT DISTINCT(DATE_FORMAT(date, '%Y %m %d')) AS todasFechas, COUNT(DISTINCT(cliente)) AS TotalClientes FROM app_prdlog.completados GROUP BY todasFechas ORDER BY date DESC LIMIT 30");



//ERRORES
$erroresGenerados = mysqli_query($conexion, "SELECT COUNT(DISTINCT(error_)) AS errorG FROM app_prdlog.errores WHERE DATE >= CURDATE()");
$imp_error1 = mysqli_fetch_array($erroresGenerados);
$imp_error = $imp_error1['errorG'];

$clientesConError = mysqli_query($conexion, "SELECT COUNT( DISTINCT (cliente)) AS clientesConError FROM app_prdlog.errores WHERE DATE >= CURDATE() ");
$imp_clte1 = mysqli_fetch_array($clientesConError);
$imp_clte = $imp_clte1['clientesConError'];




$marcas =  mysqli_query($conexion, "SELECT COUNT(DISTINCT(made)) AS marcas FROM crm_simetrical.clients");
$immarcas = mysqli_fetch_array($marcas);
$imprimirMarcas = $immarcas['marcas'];

$listaMarcas = mysqli_query($conexion, "SELECT  MADE, COUNT(MADE) AS countmade  FROM crm_simetrical.clients GROUP BY MADE");



//CLIENTES 
$queryTotales = mysqli_query($conexion, "SELECT COUNT(CLIENT) AS clientesTotales FROM crm_simetrical.groups WHERE name NOT LIKE 'DEMO %' AND NAME NOT LIKE '. %'");
$Totales = mysqli_fetch_array($queryTotales);
$clientesTotales = $Totales['clientesTotales'];

$queryRegistrados = mysqli_query($conexion, 'SELECT COUNT(DISTINCT(cliente)) as clientesRegistrados FROM app_prdlog.procesamientos WHERE DATE >= CURDATE()');
$queryFinalizados = mysqli_query($conexion, 'SELECT COUNT(DISTINCT(cliente)) AS terminados FROM app_prdlog.completados WHERE DATE >= CURDATE()');
$Registrados = mysqli_fetch_array($queryRegistrados);
$Finalizados = mysqli_fetch_array($queryFinalizados);
$clientesFinalizados = $Finalizados['terminados'];
$clientesRegistrados = $Registrados['clientesRegistrados'];
$clientesPendientes = $clientesRegistrados - $clientesFinalizados;


//ModalErrores
$modalErrorDistinto = mysqli_query($conexion, "SELECT e.Cliente, g.name, MAX(e.DATE) AS date, COUNT(e.error_) AS Duplicados, g.CLIENT AS clt2, e.error_ FROM app_prdlog.errores e
	LEFT JOIN crm_simetrical.groups g ON e.Cliente = g.CLIENT WHERE DATE >= CURDATE() GROUP BY error_ ORDER BY Cliente ASC");

// MODAL Estatus clientes finalizados
$consultaEstatus1 = mysqli_query($conexion, "SELECT p.cliente, c.cliente AS ccomple, p.date AS finicio, c.date AS ffin, p.informe,  c.informe AS estatusServer FROM app_prdlog.procesamientos p 
	right  outer JOIN app_prdlog.completados c ON p.cliente = c.cliente
	WHERE p.date >= CURDATE() AND c.date >= CURDATE() AND p.informe = 'Finalizo Sincronizacion del Cliente' GROUP BY c.date");

//-------------------------------------------------------------------------------------------------
$qCatalogo = "SELECT t1.Client, t1.Nombre, t1.Branch, t1.Paquetes, t1.TipoCliente, t1.GMT, t2.fechaProcesamientos, t1.fechaCompletado, t2.informe
FROM app_prdlog.clients_procesamientos_1 t1 LEFT JOIN app_prdlog.clients_procesamientos_2 t2 ON t1.Client = t2.Cliente";

$ejecutaCatalogo = mysqli_query($conexion, $qCatalogo);




?>