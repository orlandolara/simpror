
<div class="modal fade" id="modalConvertidos" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl modal-dialog-scrollable">
		<div class="modal-content" style="background-color: #24283C!important; color: white;">
			<div class="modal-header">
				<h6 class="modal-title" id="exampleModalLabel">INFORMACIÓN DE PAQUETES CONVERTIDOS</h6>
			</div>
			<div class="modal-body">
				<p>Información de paquetes que el MonServer ya trabajó, convirtió y los depositó en la ruta /Convertidos del Ftp interno.</p>

				<div class="container" style="align-content: center;">
					<table class="table table-sm table-striped table-borderless" style="color: white;">
						<thead style="font-size: 12px;  background-color: #24283C!important; color: white;">
							<tr>
								<th scope="col">CLIENTE</th>
								<th scope="col">PAQUETES</th>
								<th scope="col">FECHA HORA</th>
								<th scope="col">TIMESTAMP</th>        
							</tr>
						</thead>

						<tbody style="font-size: 12px!important; color: #c2c2c2;">
							<?php

							while($fila = mysqli_fetch_array($modalConvertidos))
							{
								?>
								<tr>
									<td><strong><?php echo ($fila['cliente']);?></strong></td>
									<td><?php echo ($fila['NumPaquetes']);?></td>
									<td><?php echo $fila['fechahora'] ?></td>
									<td><?php echo $fila['ts'] ?></td>
								</tr>
								<?php
							}
							?>
						</tbody>
					</table>
				</div>

			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>


<!-- solicit -->
<!-- Modal solicitudes bonito -->
<div class="modal fade" id="modalSolicitudes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl modal-dialog-scrollable">
		<div class="modal-content" style="background-color: #24283C!important; color: white;">
			<div class="modal-header">
				<h6 class="modal-title" id="exampleModalLabel">REGISTRO DE SOLICITUDES DE ENCENDIDO PRDSERVERS</h6>
			</div>
			<div class="modal-body">
			

				<div class="container">
					<div class="row">
						<div class="col-3"></div>
						<div class="col-3"></div>
						<div class="col-6">
							<ul class="list-group">
								<li class="list-group-item disabled" style="background: transparent!important; padding-top: 3px;"><strong style="color: white;">ESTADOS</strong></li>
								<li class="list-group-item" style="background: transparent!important;">
									<span><img src="images/pendiente.svg" width="19px;" style="margin-right: 10px;"><strong style="color: #FF9800;">Pendiente</strong> - Pendiente por realizar petición de lanzamiento de PrdServer.</span>
								</li>
								<li class="list-group-item" style="background: transparent!important; padding-top: 3px;">
									<span><img src="images/processing.svg" width="19px;" style="margin-right: 10px;"><strong style="color: #2196F3;">En ejecución</strong> - Realizando la ejecución de la solicitud de lanzamiento.</span>
								</li>
								<li class="list-group-item" style="background: transparent!important; padding-top: 3px;">
									<span><img src="images/correct.svg" width="19px;" style="margin-right: 10px;"><strong style="color: #1de9b6;">Completada</strong> - La solicitud fué ejecutada correctamente, PrdServer solicitado.</span>
								</li>
							</ul>
						</div>
					</div>
				</div><br>


				<hr>

				<table class="table  table-sm table-borderless table-striped">
					<thead style="font-size: 12px;  background-color: #24283C!important; color: white;">
						<tr>
							<th scope="col">CVE</th>
							<th scope="col">NOMBRE CLIENTE</th>
							<th scope="col">ESTADO</th>
							<th scope="col">REGISTRO ÚLTIMO PAQUETE</th>
							<th scope="col">SOLICITUD TOMADA</th>   
							<th scope="col">SOLICITUD COMPLETADA</th> 
							<th scope="col">¿ENCENDIÓ PRDSERVER?</th>  
						</tr>
					</thead>

					<tbody style="font-size: 12px; color: #c2c2c2;">
						<?php

						while($filaSol = mysqli_fetch_array($qmsol))
						{
							?>
							<tr>
								<td style="font-weight: bold;"><strong><?php echo ($filaSol['client']);?></strong></td>
								<td style="color: white;"><?php echo ($filaSol['name']); ?></td>

								<td>
									<?php 
									if ($filaSol['Estado'] == 0){
										echo '<span style="color: #FF9800;"><img src="images/pendiente.svg" width="19px;" style="margin-right: 10px;"> Pendiente</span>';
									} else if ($filaSol['Estado'] == 1) {
										echo '<span><img src="images/correct.svg" width="19px;" style="margin-right: 10px;"> Tomado</span>';
									} else {
										echo '<span style="color: #1de9b6;"><img src="images/correct.svg" width="19px;" style="margin-right: 10px;"> Completada</span>';
									}

									?>
								</td>

								<td><?php echo ($filaSol['Nueva']);?></td>
								<td><?php echo ($filaSol['EnProceso']);?></td>
								<td><?php echo ($filaSol['Completada']);?></td>
								<td> *ND </td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>




<!-- clientes -->

<div class="modal fade" id="modalClientes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-scrollable">
		<div class="modal-content" style="background-color: #24283C!important; color: white;">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Clientes que procesaron en SimServer´s</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<p>Información de los Servidores (Clientes) que actualmente finalizaron su procesamiento dentro de los <strong>SimServers</strong>. Este dato será temporal.</p><br>

				<table class="table table-sm">
					<thead style="font-size: 12px;  background-color: #24283C!important; color: #c2c2c2;">
						<tr>
							<th scope="col">Cliente</th>
							<th scope="col">Hora finalizó</th>    

						</tr>
					</thead>

					<tbody style="font-size: 12px; color: white;">
						<?php

						while($fila1 = mysqli_fetch_array($cltBase))
						{
							?>
							<tr>
								<td><strong><?php echo ($fila1['Client']);?></strong></td>
								<td><?php echo ($fila1['Hora']);?></td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>

			</div>
		</div>
	</div>
</div>


<!-- ERRORES -->

<div class="modal fade" id="modalErrores" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-scrollable modal-xl">
		<div class="modal-content" style="background-color: #24283C!important; color: white;">
			<div class="modal-header">
				<h6 class="modal-title" id="exampleModalLabel">REGISTRO DE ERRORES GENERADOS</h6>
			</div>
			<div class="modal-body">

				<p> Información de los errores registrados dentro del log de los PrdServers </p><br><br>
				<table class="table table-sm table-borderless table-striped">
					<thead style="font-size: 12px;  background-color: #24283C!important; color: white;">
						<tr>
							<th scope="col">CVE</th>
							<th scope="col">NOMBRE</th>
							<th scope="col" style="width: 130px!important;">HORA REGISTRO</th>    
							<th scope="col" style="width: 130px!important;">VECES DUPLICADO</th>  
							<th scope="col">DESCRIPCIÓN DEL ERROR</th>  
						</tr>
					</thead>

					<tbody style="font-size: 12px; color: #c2c2c2;">
						<?php

						while($filaErrD = mysqli_fetch_array($modalErrorDistinto))
						{
							?>
							<tr>
								<td><strong><?php echo ($filaErrD['Cliente']);?></strong></td>
								<td><?php echo ($filaErrD['name']); ?></td>
								<td><?php echo ($filaErrD['date']);?></td>
								<td><strong><?php echo ($filaErrD['Duplicados']);?></strong></td>
								<td><?php echo ($filaErrD['error_']);?></td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>



<!-- Modal ESTATUS para ver cuales clientes ya procesaron y cuanto tiempo -->
<div class="modal fade" id="modalEstatus" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-xl modal-dialog-scrollable">
		<div class="modal-content" style="background-color: #24283C!important; color: white;">
			<div class="modal-header">
				<h6 class="modal-title" id="exampleModalLabel">INFORMACIÓN DE ESTATUS DE PROCESAMIENTOS</h6>
			</div>
			<div class="modal-body">
			

				<div class="container">
			
				<table class="table  table-sm table-borderless table-striped">
					<thead style="font-size: 12px;  background-color: #24283C!important; color: white;">
						<tr>
							<th scope="col">CVE</th>
							<th scope="col">FECHA FIN</th>
							<th scope="col">INFORME DEL SERVIDOR</th>
						</tr>
					</thead>

					<tbody style="font-size: 12px; color: #c2c2c2;">
						<?php

						while($filaEst1 = mysqli_fetch_array($consultaEstatus1))
						{
							?>
							<tr>
								<td style="font-weight: bold;"><strong><?php echo ($filaEst1['cliente']);?></strong></td>
								<td style="color: white;"><?php echo ($filaEst1['ffin']); ?></td>
								<td><?php echo ($filaEst1['estatusServer']);?></td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
</div>




