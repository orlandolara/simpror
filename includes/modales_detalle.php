<!-- branchs -->
<div class="modal fade" id="modalSolicitudes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-scrollable modal-xl">
		<div class="modal-content" style="background-color: #24283C!important; color: white;">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Clientes Registrados para Procesar</h5>
			</div>
			<div class="modal-body">
				<p>Información de los clientes que regiatraron paquetes convertidos para cuando el Lanzador_prdserver se active, estos se enciendan y realicen su proceso.</p>

				<table class="table  table-sm">
					<thead style="font-size: 12px;  background-color: #24283C!important; color: #c2c2c2;">
						<tr>
							<th scope="col">Cliente</th>
							<th scope="col">Branch</th> 
							<th scope="col">Proceso</th>
							<th scope="col">Parametros</th>     
							<th scope="col">Estado</th>
							<th scope="col">Servidor</th>   
							<th scope="col">Nueva</th>
							<th scope="col">EnProceso</th>   
							<th scope="col">Completada</th>   
						</tr>
					</thead>

					<tbody style="font-size: 12px; color: white;">
						<?php

						while($filaSol = mysqli_fetch_array($qmsol))
						{
							?>
							<tr>
								<td><strong><?php echo ($filaSol['Client']);?></strong></td>
								<td><?php echo ($filaSol['Branch']);?></td>
								<td><strong><?php echo ($filaSol['Proceso']);?></strong></td>
								<td><?php echo ($filaSol['Parametros']);?></td>
								<td><strong><?php echo ($filaSol['Estado']);?></strong></td>
								<td><?php echo ($filaSol['Servidor']);?></td>
								<td><?php echo ($filaSol['Nueva']);?></td>
								<td><?php echo ($filaSol['EnProceso']);?></td>
								<td><?php echo ($filaSol['Completada']);?></td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>
			<div class="modal-footer">

			</div>
		</div>
	</div>
</div>