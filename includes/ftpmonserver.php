       <table class="table table-sm">
          <thead>
            <tr>
              <th style="color: white;">Paquete</th>
              <th style="color: white;">Date</th>
              <th style="color: white;">Tamaño</th>
            </tr>
          </thead>

          <tbody>

            <?php
            $paquetesc = getContents($contents, $var_cliente);
            $conteoc = 0;
            foreach( $paquetesc as $paqueteConvertido ){
              $buff = ftp_mdtm($conn_id, '/convertidos/'.$paqueteConvertido);

              $newbuff = date("m.d.y", $buff);


              $size = ftp_size($conn_id, '/convertidos/'.$paqueteConvertido);


              if($newbuff == $hoy){
                echo '<tr style="color: #81d4fa;><td>'.$paqueteConvertido.'</td><td>'.date("F d Y H:i:s a", $buff).'</td><td>'.$size.' Bytes </td></tr>';

              } else {
                echo '<tr style="color: #c2c2c2;"><td>'.$paqueteConvertido.'</td><td>'.date("F d Y H:i:s a", $buff).'</td><td>'.$size.' Bytes </td></tr>';
              }
              $conteoc = $conteoc + 1;
            }
          }

          ftp_close($conn_id);


          ?>

        </tbody>
      </table>

      <div class="card" style="margin-bottom: 0px; margin-top: 0px; border: 0px;">
        <div class="card-content" style="padding: 5px;">
          <div class="row" style="color: #c2c2c2;">
            <div class="col s4 center-align">
              <h5 style="color: white;"><?php echo $paquetes_esperados; ?></h5> Esperados
            </div>
            <div class="col s4 center-align" style="color: #c2c2c2;">
              <h5 style="color: white;"><?php echo $conteoc; ?></h5> Procesados
            </div>
            <div class="col s4 center-align" style="color: #e57373;">
              <h5></h5> 
            </div>
          </div>
        </div>
      </div>