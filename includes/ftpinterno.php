      <?php 
      if (!$conn_id) { 
        echo "Imposible conectar :(";
        exit; 
      } else {   ?>


        <table id="tablaPaquetesOriginales" class="table table-sm ">
          <thead>
            <tr>
              <th style="color: #c2c2c2;">Paquete</th>
              <th style="color: #c2c2c2;">Date</th>
              <th style="color: #c2c2c2;">Tamaño</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <?php 
              $hoy = date("m.d.y"); 
              ftp_pasv($conn_id, true);

              $contents = ftp_nlist($conn_id, '/convertidos');
              $contentsOriginales = ftp_nlist($conn_id, '/originales');

              $paquetesOriginales = getContents($contentsOriginales, $var_cliente);

              $conteoOriginales = 0;

             
            

              foreach( $paquetesOriginales as $paqueteOriginales ){

                $buffo = ftp_mdtm($conn_id, '/originales/'.$paqueteOriginales);
                $sizeo = ftp_size($conn_id, '/originales/'.$paqueteOriginales);

                $newbuffo = date("m.d.y", $buffo);
                $conteoOriginales = $conteoOriginales + 1;

        
              

                if($newbuffo == $hoy){
                  echo '<tr style="color: #81d4fa;"><td>'.$paqueteOriginales.'</td><td>'.date("F d Y H:i:s a", $buffo).'</td><td>'.$sizeo.' Bytes </td></tr>';

                } else {
                  echo '<tr style="color: #c2c2c2;"><td>'.$paqueteOriginales.'</td><td>'.date("F d Y H:i:s a", $buffo).'</td><td>'.$sizeo.' Bytes </td><tr>'; 
                }
              }
              ?>
            </tr>
          </tbody>
        </table>

        <div class="card" style="margin-bottom: 0px; margin-top: 0px; border: 0px;">
          <div class="card-content" style="padding: 5px;">
            <div class="row">
              <div class="col center-align" style="color: #c2c2c2;">
                <h5 style="color: white;"><?php echo $paquetes_esperados; ?></h5> Esperados
              </div>
              <div class="col center-align" style="color: #c2c2c2;">
                <h5 style="color: white;"><?php echo $conteoOriginales; ?></h5> Enviados
              </div>
              <div class="col center-align" style="color: #c2c2c2;">
                <?php 
                $resultato_pc = $conteoOriginales * 100;
                if ($resultato_pc == 0) {
                  $resultado_pc_c = 0;
                } else {
                  $resultado_pc_c = $resultato_pc / $paquetes_esperados;
                }
                
                $cien = 100;

                ?>
                <h5 style="color: white;"><?php
                if ($resultado_pc_c == $cien) {
                  echo number_format($resultado_pc_c,0).' % <i class="material-icons" style="color: #00e676;">check</i>'; 
                } else if ($resultado_pc_c > $cien) {
                  echo number_format($resultado_pc_c,0).' % <i class="material-icons" style="color: #f44336;">info_outline</i>'; 
                } else {
                  echo number_format($resultado_pc_c,0).' % <i class="material-icons" style="color: #f57f17;">clear</i>'; 
                }


                ?></h5> Cumplido
              </div>
            </div>
          </div>
        </div>