<!DOCTYPE html>
<?php 

$var_cliente = ($_GET['Cve']);
$var_nombre =  ($_GET['Grupo']);
$paquetes_esperados = ($_GET['Paquetes']);

include_once 'includes/nucleo.php';
include_once 'includes/consultas_detalle.php';

$ftp_server = "ftp.simetrical.org";
$conn_id = ftp_connect($ftp_server);
$ftp_user_name = "process";
$ftp_user_pass = "40b67c197405";
$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);

// Recorremos los archivos obtenidos
function getContents( array $contents, string $client ) {
	$paquetes = []; 
	for ($i= 0; $i < count($contents) ; $i++){
		$var_cliente = substr($contents[$i],-12,4);
  // Obtenemos el paquete de cierto cliente
		if ( $var_cliente == $client ) {
			$paquetes[] = substr($contents[$i],-12,12);
		}
	}
	return $paquetes;
}



?>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="css/catalogo.css">	
	<link rel="stylesheet" type="text/css" href="css/segunda.css">	
	<title>Simprocess - Detalle</title>
</head>
<body>

	<div class="container"><br>
		<!-- barra de navegación -->
		<nav class="navbar navbar-expand-lg shadow rounded" id="nav1">
			<a class="navbar-brand"  id="titulo1">S I M P R O C E S S</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarText">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="#" style="color: white;"> Dashboard <span class="sr-only">(current)</span></a>
					</li>
				</ul>
				<span class="navbar-text" style="padding: 0px;">
					<div class="dropdown">
						<a class="btn dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white; font-size: 11px;">
							Sesión
						</a>

						<div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
							<a class="dropdown-item" href="#" style="font-size: 12px;">Cerrar sesión</a>
						</div>
					</div>
				</span>
			</div>
		</nav> <!-- fin de barra de navegación -->

		<br>


		<!-- segunda fila columna -->

		<div class="container">
			<div class="row" style="background: #00695C; color:white; padding: 10px; border-radius: 4px;">
				<div class="col">
					<img src="images/catalogue.png" width="19px;" style="margin-right: 10px;"> 
					<strong style="color: white;"> 
						<?php echo $var_cliente.', '.$var_nombre; ?></strong>
					</div>
					<div class="col">
						<img src="images/target.svg" width="19px;" style="margin-right: 10px;"> ESTATUS  <STRONG style="color:white;">
							<?php
							if($imp_apagado >= 1) {
								echo 'FINALIZADO';
							} else if ($EstoyEn >= 1) {
								echo 'WORKING'; 
							}
							else {
								echo 'NO INICIADO';
							}
							?>
						</STRONG>
					</div>
					<div class="col">
						<img src="images/fast-time.svg" width="19px;" style="margin-right: 10px;"> Estimado 
						<STRONG style="color:white;"><?php echo $imp_tiempo; ?></STRONG>
					</div>
					<div class="col">
						<img src="images/cancel.svg" width="19px;" style="margin-right: 10px;"> Errores 
						<STRONG style="color:white;">
							<?php echo number_format($print_totalError,0); ?>
						</STRONG>
					</div>
				</div>
			</div>
			<!-- fin de la segunda fila -->
			<br><br>

			<!-- contenido general del detalle -->
			<div class="container">
				<div class="row">

					<div class="col-3" style="background-color: #24283C; border-radius: 3px;">

						<ul class="list-group">
							<li class="list-group-item d-flex justify-content-between align-items-center">
								<strong>INFORMACIÓN EXTRA</strong>
							</li>
							<li class="list-group-item d-flex justify-content-between align-items-center">
								Post y Pre Procesos
								<span class="badge"  data-toggle="modal" data-target="#modalPPP"><img src="images/plus.svg" width="19px;"></span>
							</li>
							<li class="list-group-item d-flex justify-content-between align-items-center">
								Reportes
								<span class="badge" data-toggle="modal" data-target="#modalReportes"><img src="images/plus.svg" width="19px;"></span>
							</li>
							<li class="list-group-item d-flex justify-content-between align-items-center">
								Libre1
								<span class="badge"  data-toggle="modal" data-target="#modalPPP"><img src="images/plus.svg" width="19px;"></span>
							</li>
							<hr>
							<li class="list-group-item d-flex justify-content-between align-items-center" style="border-top: 1px solid gray;">
								<strong>Sucursales</strong>  <p><strong><?php echo $imp_innactivos; ?> </strong> Activos<br>
									<strong><?php echo $imp_ActB; ?> </strong> Inactivos
								</p>
								<span class="badge" data-toggle="modal" data-target="#modalBranchs"><img src="images/plus.svg" width="19px;"></span>

							</li>
							<p style="margin-bottom: 0px; color:white; font-weight: bold; padding-left: 17px;">UBICACIÓN SUCURSALES</p>
							<li class="list-group-item d-flex justify-content-between align-items-center" style="padding-top: 0px;">

								<div id="chartdiv" style="height: 220px;"></div>	
							</li>

						</ul>
					</div>
					<div class="col-7">

						<div class="row">
							<div class="col-2">
								<div class="list-group" id="list-tab" role="tablist">
									<a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home" style="background-color: #24283C; padding-top: 20px; padding-bottom: 20px; margin-bottom: 20px;">FTPInt</a>

									<a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile" style="background-color: #24283C; padding-top: 20px; padding-bottom: 20px; margin-bottom: 20px;" >MS</a>

									<a class="list-group-item list-group-item-action" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages" style="background-color: #24283C; padding-top: 20px; padding-bottom: 20px; margin-bottom: 20px;">PrdSrvr</a>

									<a class="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings" style="background-color: #24283C; padding-top: 20px; padding-bottom: 20px;">DBU</a>
								</div>
							</div>
							<div class="col-10">
								<div class="tab-content" id="nav-tabContent">
									<div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
										<?php 
										if (!$conn_id) { 
											echo "Imposible conectar :(";
											exit; 
										} else {   ?>


											<table id="tablaPaquetesOriginales" class="table table-sm table-borderless">
												<thead>
													<tr>
														<th style="color: #c2c2c2;">PAQUETE</th>
														<th style="color: #c2c2c2;">DATE</th>
														<th style="color: #c2c2c2;">TAMAÑO</th>
													</tr>
												</thead>

												<tbody>
													<tr>
														<?php 
														$hoy = date("m.d.y"); 
														ftp_pasv($conn_id, true);

														$contents = ftp_nlist($conn_id, '/convertidos');
														$contentsOriginales = ftp_nlist($conn_id, '/originales');

														$paquetesOriginales = getContents($contentsOriginales, $var_cliente);

														$conteoOriginales = 0;




														foreach( $paquetesOriginales as $paqueteOriginales ){

															$buffo = ftp_mdtm($conn_id, '/originales/'.$paqueteOriginales);
															$sizeo = ftp_size($conn_id, '/originales/'.$paqueteOriginales);

															$newbuffo = date("m.d.y", $buffo);
															$conteoOriginales = $conteoOriginales + 1;




															if($newbuffo == $hoy){
																echo '<tr style="color: #81d4fa;"><td>'.$paqueteOriginales.'</td><td>'.date("F d Y H:i:s a", $buffo).'</td><td>'.$sizeo.' Bytes </td></tr>';

															} else {
																echo '<tr style="color: #c2c2c2;"><td>'.$paqueteOriginales.'</td><td>'.date("F d Y H:i:s a", $buffo).'</td><td>'.$sizeo.' Bytes </td><tr>'; 
															}
														}
														?>
													</tr>
												</tbody>
											</table>

											<div class="card" style="margin-bottom: 0px; margin-top: 0px; border: 0px;">
												<div class="card-content" style="padding: 5px;">
													<div class="row">
														<div class="col center-align" style="color: #c2c2c2;">
															<h5 style="color: white;"><?php echo $paquetes_esperados; ?></h5> Esperados
														</div>
														<div class="col center-align" style="color: #c2c2c2;">
															<h5 style="color: white;"><?php echo $conteoOriginales; ?></h5> Enviados
														</div>
														<div class="col center-align" style="color: #c2c2c2;">
															<?php 
															$resultato_pc = $conteoOriginales * 100;
															if ($resultato_pc == 0) {
																$resultado_pc_c = 0;
															} else {
																$resultado_pc_c = $resultato_pc / $paquetes_esperados;
															}

															$cien = 100;

															?>
															<h5 style="color: white;"><?php
															if ($resultado_pc_c == $cien) {
																echo number_format($resultado_pc_c,0).' % '; 
															} else if ($resultado_pc_c > $cien) {
																echo number_format($resultado_pc_c,0).' % '; 
															} else {
																echo number_format($resultado_pc_c,0).' % '; 
															}


															?></h5> Cumplido
														</div>
													</div>
												</div>
											</div>
										</div> <!-- fin del div del ftp -->

										<div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
											<table class="table table-sm table-borderless">
												<thead>
													<tr>
														<th style="color: white;">PAQUETE</th>
														<th style="color: white;">DATE</th>
														<th style="color: white;">TAMAÑO</th>
													</tr>
												</thead>

												<tbody>

													<?php
													$paquetesc = getContents($contents, $var_cliente);
													$conteoc = 0;
													foreach( $paquetesc as $paqueteConvertido ){
														$buff = ftp_mdtm($conn_id, '/convertidos/'.$paqueteConvertido);

														$newbuff = date("m.d.y", $buff);


														$size = ftp_size($conn_id, '/convertidos/'.$paqueteConvertido);


														if($newbuff == $hoy){
															echo '<tr style="color: #81d4fa;><td>'.$paqueteConvertido.'</td><td>'.date("F d Y H:i:s a", $buff).'</td><td>'.$size.' Bytes </td></tr>';

														} else {
															echo '<tr style="color: #c2c2c2;"><td>'.$paqueteConvertido.'</td><td>'.date("F d Y H:i:s a", $buff).'</td><td>'.$size.' Bytes </td></tr>';
														}
														$conteoc = $conteoc + 1;
													}
												}

												ftp_close($conn_id);


												?>

											</tbody>
										</table>

										<div class="card" style="margin-bottom: 0px; margin-top: 0px; border: 0px;">
											<div class="card-content" style="padding: 5px;">
												<div class="row" style="color: #c2c2c2;">
													<div class="col s4 center-align">
														<h5 style="color: white;"><?php echo $paquetes_esperados; ?></h5> Esperados
													</div>
													<div class="col s4 center-align" style="color: #c2c2c2;">
														<h5 style="color: white;"><?php echo $conteoc; ?></h5> Procesados
													</div>
													<div class="col s4 center-align" style="color: #e57373;">
														<h5></h5> 
													</div>
												</div>
											</div>
										</div>
									</div>

									<!-- prdserver-->
									<div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">

										<div class="row">
											<div class="col-12">

												<div class="list-group list-group-horizontal" id="list-tab" role="tablist">
													<a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#Log" role="tab" aria-controls="home">LOG</a>
													<a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#Paq" role="tab" aria-controls="profile">PAQUETES</a>
													<a class="list-group-item list-group-item-action" id="list-messages-list" data-toggle="list" href="#Error" role="tab" aria-controls="messages">ERRORES</a>
												</div>
											</div>
											<div class="col-12">
												<div class="tab-content" id="nav-tabContent">
													<div class="tab-pane fade show active" id="Log" role="tabpanel" aria-labelledby="list-home-list">

														<br>

														<div class="table-wrapper-scroll-y my-custom-scrollbar">
															<table class="table table-sm table-borderless table-striped mb-0">
																<thead>
																	<tr>
																		<th style="color: white;">Estatus</th>
																		<th style="color: white;">date</th>
																		<th style="color: white;">Informe</th>
																	</tr>
																</thead>

																<tbody>
																	<?php
																	while ( $filaLog = mysqli_fetch_array($executeLog)) { ?>
																		<tr>
																			<td style="color: #c2c2c2;"><?php echo $filaLog['cliente'] ?></td>
																			<td style="color: #c2c2c2;"><?php echo $filaLog['date'] ?></td>
																			<td style="color: #c2c2c2;"><?php echo $filaLog['informe'] ?></td>
																		</tr>
																	<?php } ?>
																</tbody>
															</table>
														</div>

													</div>

													<div class="tab-pane fade" id="Paq" role="tabpanel" aria-labelledby="list-profile-list">
														<br>
														<div class="card transparent" style="margin-bottom: 0px; margin-top: 0px; border: 0px;">
															<div class="card-content" style="padding: 5px;">
																<div class="row">
																	<div class="col s4 center-align">
																		<h5 style="color: white;"><?php echo $paquetes_esperados; ?></h5> Esperados
																	</div>
																	<div class="col s4 center-align">
																		<h5 style="color: white;"><?php echo number_format(round($respuestaPaqPro,0)); ?></h5> Procesados
																	</div>
																	<div class="col s4 center-align">
																		<h5 style="color: white;">
																			<?php 
																			$reglap1 = $respuestaPaqPro * 100;
																			if ($reglap1 == 0) {
																				$rreglap2 = 0;
																			} else {
																				$rreglap2 = $reglap1 / $paquetes_esperados;
																			}

																			$cien = 100;

																			if ($rreglap2 == $cien) {
																				echo $rreglap2.' % '; 
																			} else if ($rreglap2 > $cien) {
																				echo round($rreglap2,0).' % '; 
																			} else {
																				echo round($rreglap2,0).' % '; 
																			}


																			?>
																		</h5> Cumplido
																	</div>
																</div>
															</div>
														</div>

														<br>

														<table class="table table-sm table-borderless">
															<thead>
																<tr>
																	<th style="color: white;">Paquete</th>
																	<th style="color: white;">Date</th>
																	<th  style="color: white;">Tamaño</th>
																	<th  style="color: white;">Procesamientos</th>
																</tr>
															</thead>

															<tbody style="color: #c2c2c2;">
																<?php
																while ( $filaPaq = mysqli_fetch_array($exePaqtb)) { ?>
																	<tr>
																		<td><?php echo $filaPaq['cliente'] ?></td>
																		<td><?php echo $filaPaq['date'] ?></td>
																		<td><?php echo $filaPaq['informe'] ?></td>
																		<td><?php echo number_format($filaPaq['ciclado']) ?></td>
																	</tr>
																<?php } ?>
															</tbody>
														</table>

													</div>
													<div class="tab-pane fade" id="Error" role="tabpanel" aria-labelledby="list-messages-list">
														<br>
														<div class="card" style="margin-bottom: 0px; margin-top: 0px; border: 0px;">
															<div class="card-content" style="padding: 5px;">
																<div class="row">
																	<div class="col s4 center-align">
																		<h5 style="color: white;"><?php echo number_format($print_totalError,0); ?></h5> Registrados
																	</div>
																	<div class="col s4 center-align">
																		<h5 style="color: white;"><?php echo $imp_Duplicados; ?></h5> Duplicados
																	</div>
																	<div class="col s4 center-align">
																		<h5 style="color: white;"><?php echo $imp_erroresDiferentes; ?></h5> Diferentes
																	</div>
																</div>
															</div>
														</div>

														<br>

														<table class="table table-sm table-borderless">
															<thead>
																<tr style="color: white;">
																	<th>Cliente</th>
																	<th>Error</th>
																	<th>Descripción</th>
																	<th>Duplicados</th>
																</tr>
															</thead>

															<tbody style="color: #c2c2c2;">
																<?php
																while ( $filaErr = mysqli_fetch_array($executeTablaError)) { ?>
																	<tr>
																		<td><?php echo $filaErr['cliente'] ?></td>
																		<td><?php echo $filaErr['date'] ?></td>
																		<td><?php echo $filaErr['error_'] ?></td>
																		<td><?php echo $filaErr['duplicados'] ?></td>
																	</tr>
																<?php } ?>
															</tbody>
														</table>

													</div>
												</div>
											</div>
										</div>

									</div> <!-- fin del prdserver -->


									<div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">cuatro</div>
								</div>
							</div>
						</div>







					</div>

					<!-- 3ra columna -->
					<div class="col-2" style="background: #24283C; color:white; padding: 4px; border-radius: 4px;">
						<?php include_once 'includes/donde.php'; ?>
					</div>
					<!-- fin de 3ra columna -->
				</div>
			</div>

			<!-- fin del contenido general de detalle -->




		</div> <!-- fin div container general -->




		<!-- MODALES -->

		<!-- ModalPPP -->
		<div class="modal fade" id="modalPPP" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-xl">
				<div class="modal-content" style="background-color: #24283C!important; color: white;">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Modal Pre y Post Procesos Configurados</h5>
					</div>
					<div class="modal-body">
						<table class="table  table-sm table-borderless">
							<thead style="font-size: 12px;  background-color: #24283C!important; color: #c2c2c2;">
								<tr>
									<th scope="col">Tipo</th>
									<th scope="col">Orden</th> 
									<th scope="col">Momento</th>
									<th scope="col">Función</th>     
									<th scope="col">Proceso</th>
									<th scope="col">Origen</th>   
									<th scope="col">Destino</th>
									<th scope="col">Campo Clave</th>   
									<th scope="col">Estado</th>   
								</tr>
							</thead>

							<tbody style="font-size: 12px; color: white;">
								<?php
								if (!empty($ePPP)) { 
									while($filaPPP = mysqli_fetch_array($ePPP))
									{
										?>
										<tr>
											<td><strong><?php echo ($filaPPP['tipo']);?></strong></td>
											<td><?php echo ($filaPPP['orden']);?></td>
											<td><?php echo $filaPPP['momento'] ?></td>
											<td><?php echo ($filaPPP['funcion']) ?> </td>
											<td><?php echo ($filaPPP['proceso']) ?> </td>
											<td><?php echo ($filaPPP['origen']) ?> </td>
											<td><?php echo ($filaPPP['destino']) ?> </td>
											<td><?php echo ($filaPPP['campoClave']) ?> </td>
											<td><?php echo ($filaPPP['estado']) ?> </td>


										</tr>
										<?php
									}
								}
								?>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
					</div>
				</div>
			</div>
		</div>

		<!-- ModalReportes -->
		<div class="modal fade" id="modalReportes" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-xl ">
				<div class="modal-content" style="background-color: #24283C!important; color: white;">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Reportes Configurados</h5>
					</div>
					<div class="modal-body">
						Sin contenido
					</div>
					<div class="modal-footer">
					</div>
				</div>
			</div>
		</div>

		<!-- ModalBranchs -->
		<div class="modal fade" id="modalBranchs" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-xl">
				<div class="modal-content" style="background-color: #24283C!important; color: white;">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Información de Sucursales del cliente</h5>
					</div>
					<div class="modal-body">
						<table class="table  table-sm table-borderless">
							<thead style="font-size: 12px;  background-color: #24283C!important; color: #c2c2c2;">
								<tr>
									<th scope="col">Cliente</th>
									<th scope="col">Branch</th> 
									<th scope="col">Name</th>
									<th scope="col">Made</th>     
									<th scope="col">Active</th>
									<th scope="col">DMS</th>   
									<th scope="col">Ciudad</th>
									<th scope="col">Latitud</th>   
									<th scope="col">Longitud</th>   
								</tr>
							</thead>

							<tbody style="font-size: 12px; color: white;">
								<?php

								while($filaBr = mysqli_fetch_array($eBranch))
								{
									?>
									<tr>
										<td><strong><?php echo ($filaBr['CLIENT']);?></strong></td>
										<td><?php echo ($filaBr['Branch']);?></td>
										<td><?php echo $filaBr['NAME'] ?></td>
										<td><?php echo ($filaBr['Made']) ?> </td>
										<td><?php echo ($filaBr['Active']) ?> </td>
										<td><?php echo ($filaBr['DMS']) ?> </td>
										<td><?php echo ($filaBr['Ciudad']) ?> </td>
										<td><?php echo ($filaBr['Latitud']) ?> </td>
										<td><?php echo ($filaBr['Longitud']) ?> </td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
					</div>
				</div>
			</div>
		</div>

	</body>




	<!-- script de la grafica de sucursales -->
	<script type="text/javascript">
		am4core.ready(function() {

// Themes begin
am4core.useTheme(am4themes_dark);
am4core.useTheme(am4themes_animated);
// Create map instance
var chart = am4core.create("chartdiv", am4maps.MapChart);
// Set map definition
chart.geodata = am4geodata_worldLow;
// Set projection
chart.projection = new am4maps.projections.Miller();
// Create map polygon series
var polygonSeries = chart.series.push(new am4maps.MapPolygonSeries());

// Exclude Antartica
polygonSeries.exclude = ["AQ"];

// Make map load polygon (like country names) data from GeoJSON
polygonSeries.useGeodata = true;

// Configure series
var polygonTemplate = polygonSeries.mapPolygons.template;
polygonTemplate.tooltipText = "{name}";
polygonTemplate.polygon.fillOpacity = 0.6;


// Create hover state and set alternative fill color
var hs = polygonTemplate.states.create("hover");
hs.properties.fill = chart.colors.getIndex(0);

// Add image series
var imageSeries = chart.series.push(new am4maps.MapImageSeries());
imageSeries.mapImages.template.propertyFields.longitude = "longitude";
imageSeries.mapImages.template.propertyFields.latitude = "latitude";
imageSeries.mapImages.template.tooltipText = "{title}";
imageSeries.mapImages.template.propertyFields.url = "url";

var circle = imageSeries.mapImages.template.createChild(am4core.Circle);
circle.radius = 1;
circle.propertyFields.fill = "color";

var circle2 = imageSeries.mapImages.template.createChild(am4core.Circle);
circle2.radius = 1;
circle2.propertyFields.fill = "color";


circle2.events.on("inited", function(event){
	animateBullet(event.target);
})


function animateBullet(circle) {
	var animation = circle.animate([{ property: "scale", from: 1, to: 1 }, { property: "opacity", from: 1, to: 0 }], 500, am4core.ease.circleOut);
	animation.events.on("animationended", function(event){
		animateBullet(event.target.object);
	})
}

var colorSet = new am4core.ColorSet();

imageSeries.data = [
<?php while($rowB1 = mysqli_fetch_array($queryBranchMapa)) { ?>
	{
		"title": "<?php echo $rowB1['Name']; ?>",
		"latitude": <?php echo $rowB1['Latitud']; ?>,
		"longitude": <?php echo $rowB1['Longitud']; ?>,
		"color":colorSet.next()

	},
<?php }  ?>

];



}); // end am4core.ready()
</script>

</html>